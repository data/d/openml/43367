# OpenML dataset: COVID-19-Indonesia-Dataset

https://www.openml.org/d/43367

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The COVID-19 dataset in Indonesia was created to find out various factors that could be taken into consideration in decision making related to the level of stringency in each province in Indonesia.
Content
Data compiled based on time series, both on a country level (Indonesia), and on a province level. If needed in certain provinces, it might also be provided at the city / regency level.
Demographic data is also available, as well as calculations between demographic data and COVID-19 pandemic data.
Acknowledgements
Thank you to those who have provided data openly so that we can compile it into a dataset here, which is as follows: covid19.go.id, kemendagri.go.id, bps.go.id, and bnpb-inacovid19.hub.arcgis.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43367) of an [OpenML dataset](https://www.openml.org/d/43367). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43367/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43367/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43367/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

